using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum estadodejuego
    { 
        gameOver,
        play,
        pause,
        kill,
        win
    }




public class GameManager : MonoBehaviour
{


    [Tooltip("Mensaje en pantalla")]
    [SerializeField] GameObject mensaje;

    [Tooltip("Mensaje en pantalla")]
    [SerializeField] GameObject pausa;

    [Tooltip("Mensaje en pantalla")]
    [SerializeField] GameObject start;

    [Tooltip("Mensaje en pantalla")]
    [SerializeField] GameObject win;

    [Tooltip("Vida 1")]
    [SerializeField] GameObject Vida1;

    [Tooltip("Vida 2")]
    [SerializeField] GameObject Vida2;

    [Tooltip("Vida 3")]
    [SerializeField] GameObject Vida3;


    [Tooltip("Enemigos")]
    [SerializeField] GameObject[] enemies;

    public int numeroDeEnemigos;





    AudioSource audioSource;

    private int points = 0;
    private int vidas=4;
    public estadodejuego estadoActual;

    public static GameManager sharedInstance;
    private void Awake()
    {
        if (sharedInstance)
            Destroy(this);
        else
            sharedInstance = this;

        audioSource = GetComponent<AudioSource>();

        pausa.SetActive(false);
        win.SetActive(false);
    }



    // Start is called before the first frame update
    void Start()
    {

       
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        numeroDeEnemigos = enemies.Length;
        estadoActual = estadodejuego.gameOver;
        mensaje.SetActive(false);

    }

    public void EnemigosMenos()
    {
        numeroDeEnemigos--;
        if (numeroDeEnemigos == 0)
        {
            mensaje.GetComponentInChildren<Text>().text = "Well Done";
            mensaje.SetActive(true);
            estadoActual = estadodejuego.gameOver;
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EmpezarJuego()
    {

        if (estadoActual == estadodejuego.gameOver)
        {
            estadoActual = estadodejuego.play;
            if (mensaje)
            {
                start.SetActive(false);
            }
            if (pausa)
            {
                pausa.SetActive(false);
            }

            if (audioSource)
            {
                audioSource.Play();
            }

            foreach (GameObject enemy in enemies)
            {
                enemy.SetActive(true);
                enemy.GetComponent<EnemyController>().ReiniciarPosicion();
            }


        }


        /**
        else if (estadoActual == estadodejuego.play)
            estadoActual = estadodejuego.pause;
        else
            estadoActual = estadodejuego.play; **/
    }



    public void kill()
    {
        GameOver();

        if (vidas == 3)
        {
            Vida3.SetActive(false);
            Invoke(nameof(RestartGame), 2f);
        }
        else if (vidas == 2)
        {
            Vida2.SetActive(false);
            Invoke(nameof(RestartGame), 2f);
        }

        else if (vidas == 1)
            {
            Vida1.SetActive(false);
            Invoke(nameof(RestartGame), 2f);
        }

        foreach (GameObject enemy in enemies)
            enemy.SetActive(true);
    }

    void RestartGame()
    {
        PlayerController.sharedInstance.RestartPlayer();
        estadoActual = estadodejuego.play;
    }

    public void PauseGame()
    {
        if (estadoActual == estadodejuego.gameOver)
            return;
        else if (estadoActual == estadodejuego.play)
        {
            estadoActual = estadodejuego.pause;
            pausa.SetActive(true);
            audioSource.Pause();
        }
        else if (estadoActual == estadodejuego.pause)
        {
            estadoActual = estadodejuego.play;
            pausa.SetActive(false);
            audioSource.Pause();
        }
    }

    public void GameOver()
    {
        SceneManager.LoadScene(0);

        estadoActual = estadodejuego.kill;
        PlayerController.sharedInstance.PKill();
        vidas--;
        if (vidas == 0)
        {
            estadoActual = estadodejuego.gameOver;
            mensaje.SetActive(true);
            if (audioSource)
            {
                audioSource.Stop();
            }

            PlayerController.sharedInstance.RestartPlayer();
            Vida3.SetActive(true);
            Vida2.SetActive(true);
            Vida1.SetActive(true);
        }
    }

    
    public void Points()
    {
        points++;
    }

}
