using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController sharedInstance;

    //Asignar Arma
    [Tooltip("El Gancho que Dispara")]
    [SerializeField] GameObject Gancho;

    [Tooltip("El sonido que hace el gancho")]
    [SerializeField] AudioClip sonidoGancho;

    [Tooltip("Velocidad")]
    [Range(0,50)]
    [SerializeField] float speed= 20f;

    // Variable Spite Render
    private SpriteRenderer spriteRenderer;

    // Variable Transform
    public Transform playerTransform;

    // Variable Animator
    private Animator playerAnimator;


    // Variable 
    bool estaAlFrente = false;

    // Variable
    bool Dispara = false;

    // Fix Shoot && Walk
    bool disparando = false;
    private Rigidbody2D rb;

    private AudioSource audioSource;


    private CapsuleCollider2D capsuleCollider;

    bool parpadeando = false;

    Vector3 initialPos;


    public LayerMask layer;

    void pararDisparo()
    {
        disparando = false;

    }
 

    /** IEnumerator pararDisparo()
    {
        yield return new WaitForSeconds(0.35f);
        disparando = false;
    } **/





    // Desde el momento en el que se crea el objeto
    private void Awake()
    {
        if (sharedInstance)
            Destroy(this);
        else
            sharedInstance = this;


        Application.targetFrameRate = 60; //FPS 75

        // Obtener componente del Juego
        playerAnimator = gameObject.GetComponent<Animator>();
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        rb = gameObject.GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();
        capsuleCollider = GetComponent<CapsuleCollider2D>();

        initialPos = gameObject.transform.position;
    }


    public void PKill()
    {
        rb.velocity = Vector2.zero;
        capsuleCollider.enabled = false;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (parpadeando)
        {
            spriteRenderer.enabled = !spriteRenderer.enabled;
        }


        if(GameManager.sharedInstance.estadoActual != estadodejuego.play)
                return;


        /** if (playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("Disparando"))
            return; **/

        playerAnimator.SetBool("Camina", false);

        float valor = Input.GetAxisRaw("Horizontal");


        if (!disparando && Input.GetButtonDown("Fire1"))
        // if (Input.GetButtonDown("Jump"))
        {
            disparando = true;
            playerAnimator.SetTrigger("Dispara");
            playerAnimator.SetBool("Camina", false);
            // invoke(nameof(pararDisparo),0.635f); Invoke
            // StartCoroutine(pararDisparo()); Corrutina
            Shoot();
        }


       if (disparando)
       return;


        // Pulsar letra A Izquierda
        //if (Input.GetKey(KeyCode.A)) {
        if (valor<0)
        {
            
            mover(valor);



            // playerTransform.position = new Vector3(playerTransform.position.x - 0.1f,
            // playerTransform.position.y, playerTransform.position.z);

            /** Flip con escala
             if (estaAlFrente)
             {
                 playerTransform.localScale = new Vector3(
                     1, playerTransform.localScale.y, playerTransform.localScale.z);
                 estaAlFrente = false;            }
            **/


            if (!estaAlFrente)
            {
                spriteRenderer.flipX = !spriteRenderer.flipX;
                estaAlFrente = true;
            }



            playerAnimator.SetBool("Camina", true);

        }

        // Pulsar Letra D Derecha

        // if (Input.GetKey(KeyCode.D))
        if (valor>0)
        {
            mover(valor);

            // playerTransform.position = new Vector3(playerTransform.position.x + 0.1f,
            // playerTransform.position.y, playerTransform.position.z);


            /** Flip con escala
             if (!estaAlFrente)
             {
                 playerTransform.localScale = new Vector3(
                     1, playerTransform.localScale.y, playerTransform.localScale.z);
                 estaAlFrente = true;
             }
            **/

            if (estaAlFrente)
            {
                spriteRenderer.flipX = !spriteRenderer.flipX;
                estaAlFrente = false;
            }

            playerAnimator.SetBool("Camina", true);


            RaycastHit2D hit = Physics2D.Raycast(new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + 2f), Vector2.up, 10f, layer);
            if (hit.collider)
            {
                print(hit.collider.name);
                Debug.DrawLine(new Vector2(gameObject.transform.position.x, gameObject.transform.position.y +2f), new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + 12f), Color.red);
              
            }
            else
                Debug.DrawLine(new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + 2f), new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + 12f), Color.blue);


        }

    }

    private void mover(float direccion)
        {
           rb.MovePosition(new Vector2(rb.position.x + (direccion * speed * transform.right.x *Time.deltaTime), rb.position.y));

           //rb.velocity = new Vector2(rb.position.x + (direccion * speed * transform.right.x * Time.deltaTime), rb.velocity.y);
    }

    
    private void Shoot()
        {
            Vector2 harpoonPosition = transform.position;

            if (!estaAlFrente)
                harpoonPosition = new Vector2(harpoonPosition.x + 0.1f, harpoonPosition.y - 8.95f);
            else
                harpoonPosition = new Vector2(harpoonPosition.x - 0.3f, harpoonPosition.y - 8.95f);

            GameObject harpoonInstance = Instantiate(Gancho, harpoonPosition, Quaternion.identity);

        if(audioSource && sonidoGancho)
        {
            audioSource.PlayOneShot(sonidoGancho);
        }
    }

    public void RestartPlayer()
    {
        capsuleCollider.enabled = true;
        rb.velocity = Vector2.zero;
        gameObject.transform.position = initialPos;
        StartCoroutine(FlashPlayer());
    }

    IEnumerator FlashPlayer()
    {
        parpadeando = true;
        yield return new WaitForSeconds(2f);
        parpadeando = false;
        spriteRenderer.enabled = true;
        yield return new WaitForSeconds(0.5f);
    }





}
