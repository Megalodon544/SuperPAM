using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsController : MonoBehaviour
{


    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Harpoon(Clone)")
        {
            GameManager.sharedInstance.Points();
        }
    }
}
