using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeController : MonoBehaviour
{


    private IEnumerator coroutine;
    int segundos = 120;
    Text tiempo;

    IEnumerator Contador()
    {
        while (segundos > 0 )
        {
            yield return new WaitForSeconds(1f);
            if (GameManager.sharedInstance.estadoActual == estadodejuego.play)
            segundos--;
            tiempo.text = "Tiempo: " + segundos;
        }
        // GameManager.sharedInstance.kill();
        GameManager.sharedInstance.GameOver();
        segundos = 120;
    } 

    // Start is called before the first frame update
    void Start()
    {
        tiempo = GetComponent<Text>();
        coroutine = Contador();
        StartCoroutine(coroutine);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.sharedInstance.estadoActual == estadodejuego.gameOver)
        {
            StopCoroutine(coroutine);
            segundos = 120;
            coroutine = Contador();
            StartCoroutine(coroutine);
        }
    }



}
