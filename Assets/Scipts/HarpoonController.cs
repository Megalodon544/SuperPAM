using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HarpoonController : MonoBehaviour
{

    [Header("Caracteristicas del gancho")]
    [Range(1,50)] [Tooltip("Velocidad del Gancho que dispara")] [SerializeField] int SpeedHarpoon = 5;


    Rigidbody2D _rigidbody2D;


    // Start is called before the first frame update

    private void Awake()
    {
        _rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Debug.Log("ha entrado: " + collision.name);
        if (collision.name.Equals("Ceiling"))
        {
            _rigidbody2D.velocity = Vector2.zero;
            SpeedHarpoon = 0;
            Destroy(this.gameObject, 0.50f);
        }

        if (collision.CompareTag("Points"))
        {
            _rigidbody2D.velocity = Vector2.zero;
            SpeedHarpoon = 0;
            Destroy(this.gameObject, 0.50f);
            Destroy(collision.gameObject);
        }
        if (collision.CompareTag("Enemy"))
        {
            _rigidbody2D.velocity = Vector2.zero;
            SpeedHarpoon = 0;
            collision.gameObject.SetActive(false);
            GameManager.sharedInstance.EnemigosMenos();
            Destroy(collision.gameObject);
        }
    }

    void Start()
    {
        
    }

    private void Update()
    {
    }

    // Update is called once per frame
    void FixedUpdate()
    {
      _rigidbody2D.velocity = new Vector2(0f, SpeedHarpoon);

    }
}
