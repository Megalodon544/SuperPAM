using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseController : MonoBehaviour
{
    public void Pause()
    {
        GameManager.sharedInstance.PauseGame();
    }
}

