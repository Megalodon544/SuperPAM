using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    public Vector2 inicitialPos;

    private Rigidbody2D rb;



    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

    }

    private void Awake()
    {
        inicitialPos = this.transform.position; ;
    }


    // Update is called once per frame
    void Update()
    {
        if (GameManager.sharedInstance.estadoActual != estadodejuego.play && rb.bodyType == RigidbodyType2D.Dynamic)
        {
            rb.bodyType = RigidbodyType2D.Static;
        }
        else if (GameManager.sharedInstance.estadoActual == estadodejuego.play && rb.bodyType != RigidbodyType2D.Dynamic)
        {
            rb.bodyType = RigidbodyType2D.Dynamic;
        }

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            GameManager.sharedInstance.kill();

            this.transform.position = inicitialPos;
            rb.velocity = Vector2.zero;
        }
    }

    public void ReiniciarPosicion()
    {
        gameObject.transform.position = inicitialPos;
    }
}
